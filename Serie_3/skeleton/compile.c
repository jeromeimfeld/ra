/*
 * authors:   Jérôme Imfeld
 *            Alexander Campbell
 * modified:  2016-04-16
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include "memory.h"
#include "mips.h"
#include "compiler.h"

int main ( int argc, char** argv ) {
    /* Task (c) implement main */
    if (argc != 3) {
        printf("Usage: %s\n expression filename", *argv);
        return EXIT_FAILURE;
    }

    verbose = TRUE;
    printf("%-9s%s\n%-9s", "Input:", argv[1], "Postfix:");
    compiler(argv[1], argv[2]);
    printf("\nMips Binary saved to %4s\n", argv[2]);
    return EXIT_SUCCESS;
}
