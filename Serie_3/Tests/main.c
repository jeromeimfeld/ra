#include <stdlib.h>
#include <stdio.h>

void callA(int a) {
 printf("A: %i\n", a);
}

void callB(int a) {
 printf("B: %i\n", a);
}

void callC(int a) {
 printf("C: %i\n", a);
}

void (*functionPointer[3])(int) = {
  &callA, &callB, &callC
};

void mul(short x[], int index, int mul) {
  x[index] *= mul;
}

int main (int argc, const char * argv[]) {
	printf("\n\n\n\nUbung1:\n\n");

	functionPointer[0](10);
	functionPointer[1](11);
	functionPointer[2](12);

	printf("\n\n\n\nUbung2:\n\n");

	int x, z;
	int const y = 2;
	x = 1; z = 3;

	int *a;
	int const *b;
	int *const c = &z;
	int const *const d;

	a = &x;
	b = &y;
	b = &z;
	z = 5;

	printf("%d\n", *a);
	printf("%d\n", *b);



	printf("\n\n\n\nUbung4:\n\n");

  int t = 15;
  while (t > 0) {
    t--;
  }
  printf("Done t = %d\n", t);



  printf("\n\n\n\nUbung7:\n\n");

  short u[3] = {2, 3, 5};
  mul(u, 1, 2);

  printf("Done u[x] = %d\n", u[1]);
}
